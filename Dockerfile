# Start from fresh ubuntu focal & add some tools
# note: rsyslog & curl (openssl,etc) needed as dependencies too
FROM ubuntu:focal


# File Author / Maintainer
MAINTAINER huezohuezo1990 <huezohuezo.1990@gmail.com>

# datos del Usuario
ARG USUARIO=huezo
ARG UID=1990
ARG GID=1990
# Clave para el Usuario
ARG CLAVE=huezo
#Creacion Usuario
RUN useradd -m ${USUARIO} --uid=${UID} && echo "${USUARIO}:${CLAVE}" | chpasswd
#Cambiar de /bin/sh a /bin/bash 
RUN usermod -s /bin/bash ${USUARIO}
# crear $HOME
#USER ${UID}:${GID}
#WORKDIR /home/${USUARIO}


RUN apt update
RUN apt install -y rsyslog nano curl
#Usuario de Ubuntu como sudo
RUN apt install sudo -y
RUN usermod -aG sudo ${USUARIO}
# SUDO sin Clave
RUN sed -i '/%sudo[[:space:]]/ s/ALL[[:space:]]*$/NOPASSWD:ALL/' /etc/sudoers


# Download BI Connector to /mongosqld
WORKDIR /tmp
RUN curl https://info-mongodb-com.s3.amazonaws.com/mongodb-bi/v2/mongodb-bi-linux-x86_64-ubuntu2004-v2.14.4.tgz -o bi-connector.tgz && \
    tar -xvzf bi-connector.tgz && rm bi-connector.tgz && \
    mv /tmp/mongodb-bi-linux-x86_64-ubuntu2004-v2.14.4 /mongosqld

# Setup default environment variables
ENV MONGODB_HOST mongodb
ENV MONGODB_PORT 27017
ENV LISTEN_PORT 3307
RUN  ln -sf /dev/stdout /var/log/mongosqld.log 
RUN  ln -sf /dev/stderr /var/log/error.log 
# Start Everything
# note: we need to use sh -c "command" to make rsyslog running as deamon too
RUN service rsyslog start
CMD sh -c "/mongosqld/bin/mongosqld --logPath /var/log/mongosqld.log --mongo-uri mongodb://$MONGODB_HOST:$MONGODB_PORT/?connect=direct --addr 0.0.0.0:$LISTEN_PORT"

